#!/usr/local/bin/python

from distutils.core import setup

setup(name = 'piacin',
	  version = '1.0',
	  description = "NIA3CIN for PyPy JIT Parameters",
	  author = "Shin Yoo",
	  author_email = "shin.yoo@ucl.ac.uk",
	  packages = ['piacin', 'piacin.hc', 'piacin.avm'])